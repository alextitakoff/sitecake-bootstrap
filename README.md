# sitecake-bootstrap

Drop dead simple way to get an editable site up and running. Includes bootstrap 2.3.2, sitecake 1.0.41. 

To use:

* Install nginx, php5-fpm
* Create web directory in home dir
* Clone repo into web directory
* mkdir web/sitecake-content
* chgrp www-data web/sitecake-content
* Set up nginx:

Example nginx config:

	server {
		listen 80 default_server;
		listen [::]:80 default_server ipv6only=on;

		root /home/reuben/web;
		index index.php;

        	location / {
	                try_files $uri $uri/ /index.html;
	        }

	        error_page 404 /404.html;

	        error_page 500 502 503 504 /50x.html;
	        location = /50x.html {
	              root /usr/share/nginx/www;
	        }

	        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
	        location ~ \.php$ {
	                try_files $uri =404;
	                fastcgi_pass unix:/var/run/php5-fpm.sock;
	                fastcgi_index index.php;
	                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
	                include fastcgi_params;
                
	        }
	}

* Restart nginx
* Go to url /, verify that it renders
* Go to url /?login, and use password "admin" to login (Note: to change the password, you need to give www-data write permission to credential.php; change it back afterwards)

## Changing the template

Visit http://startbootstrap.com/ to get new bootstrap templates; http://bootswatch.com/ to get new CSS.

## Sitecake

* index.php must have as its first line the hook into sitecake. 
* CMS Editable divs are marked with a class of sc-content-whateverid.
* http://sitecake.com/


